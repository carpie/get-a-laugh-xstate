import React from 'react';
import './styles.css';
import { LoadingSpinner } from './LoadingSpinner';

import { Machine, assign } from 'xstate';
import { useMachine } from '@xstate/react';

import { fetchLaughs } from './fetchLaughs';

const laughMachine = new Machine({
  context: {
    comicImg: '',
    fetchCount: 0,
    error: ''
  },
  initial: 'firstView',
  states: {
    firstView: {
      on: {
        GET_A_LAUGH: 'loading'
      }
    },
    loading: {
      entry: ['incrementFetchCounter'],
      invoke: {
        src: 'fetchLaughs',
        onDone: {
          target: 'comicView',
          actions: ['storeComic']
        },
        onError: {
          target: 'errorView',
          actions: ['storeError']
        }
      }
    },
    comicView: {
      on: {
        GET_A_LAUGH: 'loading'
      }
    },
    errorView: {
      on: {
        GET_A_LAUGH: 'loading'
      }
    }
  }
}, {
  actions: {
    incrementFetchCounter: assign({
      fetchCount: ({ fetchCount }) => fetchCount + 1
    }),
    storeComic: assign({
      comicImg: (_, event) => event.data.img
    }),
    storeError: assign({
      error: (_, event) => event.data.message
    })
  },
  services: {
    fetchLaughs
  }
});

export default function App() {
  const [state, send] = useMachine(laughMachine, { devTools: true });

  function getALaugh() {
    console.log('laugh requested');
    send('GET_A_LAUGH');
  }

  return (
    <div className="App">
      <div className="App__actionArea">
        <button onClick={getALaugh}>Get A Laugh</button>
        <div>
          Laughs requested: {state.context.fetchCount}
        </div>
      </div>
      <div className="App__comics">
        {state.matches('firstView') && (
          <h2>Click the button to get a much needed laugh!</h2>
        )}
        {state.matches('loading') && (
          <LoadingSpinner />
        )}
        {state.matches('comicView') && (
          <div>
            <img src={state.context.comicImg} alt="A much needed laugh" />
          </div>
        )}
        {state.matches('errorView') && (
          <>
            <h1>Request to laugh denied!</h1>
            <h2>{state.context.error}</h2>
          </>
        )}
      </div>
    </div>
  );
}
