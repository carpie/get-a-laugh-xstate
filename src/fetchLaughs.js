const percentChance = percent => {
  const num = Math.floor(Math.random() * 100 + 1);
  return num <= percent;
};

export const fetchLaughs = () => {
  const comicId = Math.floor(Math.random() * 2035 + 1);
  return new Promise((resolve, reject) => {
    // This does the actual fetching of data
    const actualFetch = () => {
      fetch(`https://xkcd.now.sh/?comic=${comicId}`)
        .then(resp => {
          if (resp.ok) {
            return resp.json();
          }
          throw new Error(`${resp.statusCode} - ${resp.statusText}`);
        })
        .then(data => {
          // Even if this is good, we'll thrown an error 25% of the time anyway
          if (percentChance(25)) {
            return reject(new Error("No laughs for you!"));
          }
          return resolve(data);
        });
    };
    // This function has a 20% chance of having a long delay before loading
    if (percentChance(20)) {
      setTimeout(actualFetch, 5000);
    } else {
      actualFetch();
    }
  });
};
